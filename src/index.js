const path = require('path')
const { Client, Routes, Collection, InteractionType, GatewayIntentBits } = require('discord.js')
const { registerCommands, registerSubcommands, registerEvents } = require('./utils/registry')
const { readFileSync } = require('fs')

//var util = require('util')


global.client = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildVoiceStates,
        GatewayIntentBits.MessageContent
    ],
    disableMentions: 'everyone',
    rest: { version: '10' }
})

if (process.argv.length > 2)
    client.type = process.argv[2]
client.config = JSON.parse(readFileSync('src/config.json'))
client.rest.setToken(client.type == 'dev'
    ? client.config.app.dev_token
    : client.config.app.token)
client.app_id = client.type == 'dev'
    ? client.config.app.dev_app_id
    : client.config.app.app_id
const GUILD_ID = client.config.app.guild
const guilds = client.type == 'dev'
    ? client.config.opt.guilds_dev
    : client.config.opt.guilds
// END OF CONFIGURATIONS
///////////////////////////


async function main() {
    try {
        client.slashCommands = new Collection()
        client.slashSubcommands = new Collection()
        await registerCommands(client)
        await registerSubcommands(client)
        await registerEvents(client)
        // map all found SlashCmds
        const slashCommandsJson = client.slashCommands.map((cmd) => cmd.getSlashCommandJSON())
        const slashSubcommandsJson = client.slashSubcommands.map((cmd) => cmd.getSlashCommandJSON())

        /////////////////////////////////////////////
        // global cmd clean /////////////////////////
        // somehow faster than applicationGuildCommands in cache
        client.rest.put(Routes.applicationCommands(client.app_id), {
            body: [],
            //body: [ ...slashCommandsJson ],
        })
          //.then(() => console.log('Successfully deleted all application commands.'))
          .catch(console.error)

        // 1 server
        if (!guilds.length) {
            //////////////////////////////////
            // guild cmd clean ///////////////
            // client.rest.put(Routes.applicationGuildCommands(client.app_id, GUILD_ID), { body: [] })
            //   .then(() => console.log('Successfully deleted all guild commands.'))
            //   .catch(console.error)
            client.rest.put(Routes.applicationGuildCommands(client.app_id, GUILD_ID), {
                body: [ ...slashCommandsJson, ...slashSubcommandsJson ],
            }).catch(err => console.log(err))
        // all servers
        } else {
            var i = 1
            guilds.forEach(guild => {
                //////////////////////////////////////////
                // guild cmd clean ///////////////////////
                // client.rest.put(Routes.applicationGuildCommands(client.app_id, guild), {
                //     body: []
                // })
                // .then(() => console.log('Successfully deleted all guild commands.'))
                // .catch(console.error)

                //console.log(i++, guild, typeof(guild))
                client.rest.put(Routes.applicationGuildCommands(client.app_id, guild), {
                    body: [ ...slashCommandsJson, ...slashSubcommandsJson ],
                })
                //.then(console.log(`Successfully re-added all guild commands; gid: ${guild}`))
                .catch(err => console.log(err))
            })
        }
        // Toggle Modes
        await client.login(client.type == 'dev' ? client.config.app.dev_token : client.config.app.token)
    } catch (err) {
        console.log(err)
    }
}

main()