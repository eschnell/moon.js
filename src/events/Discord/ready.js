const { writeFileSync } = require('fs')

const { useMainPlayer } = require('discord-player')

module.exports = {
    name: 'ready',
    once: true,
    execute(client) {
        const guilds = client.guilds.cache.map(guild => guild.id)
        client.config.opt.guilds = guilds
        if (client.type == 'dev') { writeFileSync('src/config_dev.json', JSON.stringify(client.config, null, 4)) }
        else { writeFileSync('src/config.json', JSON.stringify(client.config, null, 4)) }

        // SHOW BOT CONFIGS
        console.log(`\n======================================`)
        console.log(`Logged to the client ${client.user.username}#${client.user.discriminator}`)
        console.log(`Connected to ${client.config.opt.guilds.length} discord server(s)\nLets play some music!`)
        console.log(`======================================\n`)
        client.user.setActivity(client.config.app.playing)

        // player debug
        if (process.env.DEBUG === true) {
            const player = useMainPlayer()
            // generate dependencies report
            console.log(player.scanDeps())
            // log metadata query, search execution, etc.
            player.on('debug', console.log)
            // log debug logs of the queue, such as voice connection logs, player execution, streaming process etc.
            player.events.on('debug', (queue, message) => console.log(`[DEBUG ${queue.guild.id}] ${message}`))
        }
    }
}