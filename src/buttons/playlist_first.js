const makePlaylistEmbed = require('../utils/makePlaylistEmbed')

module.exports = async function PlaylistFirstButton(inter) {
    await makePlaylistEmbed(inter, 0)
}