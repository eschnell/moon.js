const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue  } = require('discord-player')

module.exports = class RemoveSlashCommand extends BaseSlashCommand {
    // name: 'remove',
    // description: 'Remove a song from the queue',
    // category: 'music',
    // voiceChannel: true,
    // options: [
    //     {
    //         name: 'song',
    //         description: 'the name/url of the track you want to remove',
    //         type: ApplicationCommandOptionType.String,
    //         required: false,
    //     },
    //     {
    //         name: 'number',
    //         description: 'the place in the queue the song is in',
    //         type: ApplicationCommandOptionType.Number,
    //         required: false,
    //     }
    // ],
    constructor() {
        super('remove', 'Remove a song from the queue', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const number =  inter.options.getNumber('number')
        const track = inter.options.getString('song')
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!track && !number)
            inter.editReply({
                content: `You have to use one of the options to remove a song ${inter.member}... try again ? ❌`
            })
        const BaseEmbed = new EmbedBuilder()
            .setColor('#2f3136')

        if (track) {
            const track_to_remove = queue.tracks.toArray()
                .find((t) => t.title === track || t.url === track)
            if (!track_to_remove)
                return inter.editReply({
                    content: `could not find ${track} ${inter.member}... try using the url or the full name of the song ? ❌`
                })
            queue.removeTrack(track_to_remove)
            BaseEmbed.setAuthor({name: `removed ${track_to_remove.title} from the queue ✅` })
            return inter.editReply({ embeds: [BaseEmbed] })
        }
        if (number) {
            const index = number - 1
            const trackname = queue.tracks.toArray()[index].title

            if (!trackname)
                return inter.editReply({
                    content: `This track dose not seem to exist ${inter.member}...  try again ?❌`
                })
            queue.removeTrack(index)
            BaseEmbed.setAuthor({ name: `removed ${trackname} from the queue ✅` })
            return inter.editReply({ embeds: [BaseEmbed] })
        }
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addIntegerOption(opt => opt
                .setName('number')
                .setDescription('The place in the queue the song is in')
                .setMinValue(1)
                .setRequired(true))
            .toJSON()
    }
}
