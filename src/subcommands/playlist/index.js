const { SlashCommandBuilder } = require('discord.js')
const BaseSlashSubcommand = require('../../utils/BaseSlashSubcommand')

module.exports = class PlaylistSubcommand extends BaseSlashSubcommand {
    constructor() {
        super(

            //name
            'playlist',
            [],
            //subCommands
            ['load', 'save', 'view']
        )
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName('playlist')
            .setDescription('Manage your playlists')

            .addSubcommand(subcmd => subcmd
                .setName('load')
                .setDescription('Loads a playlist')
                .addStringOption(opt => opt
                    .setName('name')
                    .setDescription('The name of the playlist to load')
                    .setRequired(true))
            )

            .addSubcommand(subcmd => subcmd
                .setName('save')
                .setDescription('Saves a playlist')
                .addStringOption(opt => opt
                    .setName('name')
                    .setDescription('The name to give to the playlist')
                    .setRequired(true))
            )

            // .addSubcommand(subcmd => subcmd
            //     .setName('remove')
            //     .setDescription('Deletes a playlist')
            //     .addStringOption(opt => opt
            //         .setName('name')
            //         .setDescription('The name of the playlist to remove')
            //         .setRequired(true)
            //     )
            // )

            .addSubcommand(subcmd => subcmd
                .setName('view')
                .setDescription('View saved playlists')
            )
        }
}
