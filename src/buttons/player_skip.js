const { useQueue } = require('discord-player')

module.exports = async function SkipButton(inter) {
    await inter.deferReply({ ephemeral: true })
    let queue = useQueue(inter.guild)

    if (!queue || !queue.isPlaying())
        return inter.editReply({ content: `No music currently playing... try again ? ❌` })
    let success = queue.node.skip()

    if (success) {
        queue.setMetadata({
            channel: queue.metadata['channel'],
            np: null
        })
        inter
            .editReply({
                content: `\`Current music ${queue.currentTrack.title} skipped ✅\``
            })
            .then(setTimeout(() => inter.deleteReply(), 5000))
    } else {
        queue.metadata['channel']
            .send({ content: `Something went wrong ${inter.member}... try again ? ❌` })
            .then(msg => setTimeout(() => msg.delete(), 10000))
    }
}