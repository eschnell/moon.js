const { useQueue, QueueRepeatMode } = require('discord-player')
//var util = require('util')

module.exports = async function LoopButton(inter) {
    await inter.deferReply({ ephemeral: true })
    var queue = new useQueue(inter.guild)
    let methods = ['off :arrow_right:', 'track 🔂', 'queue 🔁']

    if (!queue || !queue.isPlaying())
        return inter.editReply({
            content: `No music currently playing... try again ? ❌`
        })
    let repeatMode = queue.repeatMode
    if (repeatMode === 0 || repeatMode === -1)
        queue.setRepeatMode(QueueRepeatMode.TRACK)                     //TRACK 1
    if (repeatMode === 1)
        queue.setRepeatMode(QueueRepeatMode.QUEUE)                     //QUEUE 2
    if (repeatMode === 2)
        queue.setRepeatMode(QueueRepeatMode.OFF)                       //OFF   0
    await inter.editReply({
        content: `Loop has been set to **${methods[queue.repeatMode]}**`
    }).then(setTimeout(() => inter.deleteReply(), 30000))
}