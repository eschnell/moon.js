const makePlaylistEmbed = require('../utils/makePlaylistEmbed')

module.exports = async function PlaylistLastButton(inter) {
    const em = inter.message.embeds[0]
    const max_page = em.footer['text'].split(' ')[3]
    await makePlaylistEmbed(inter, parseInt(max_page) - 1)
}