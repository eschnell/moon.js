const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class VolumeSlashCommand extends BaseSlashCommand {
    // name: 'volume',
    // description: 'Adjust volume',
    // category: 'music',
    // voiceChannel: true,
    // options: [
    //     {
    //         name: 'volume',
    //         description: 'the amount volume',
    //         type: ApplicationCommandOptionType.Number,
    //         required: true,
    //         minValue: 1,
    //         maxValue: maxVol
    //     }
    // ],
    constructor() {
        super('volume', 'Adjust global volume', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue)
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        const vol = inter.options.getNumber('volume')

        if (queue.node.volume === vol)
            return inter.editReply({
                content: `The volume you want to change is already the current one ${inter.member}... try again ? ❌`
            })
        const success = queue.node.setVolume(vol)
        const maxVol = client.config.opt.maxVol

        return inter.editReply({
            content:
                success
                ? `The volume has been modified to ${vol}/${maxVol}% 🔊`
                : `Something went wrong ${inter.member}... try again ? ❌`
            })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addIntegerOption(opt => opt
                .setName('volume')
                .setDescription('The volume you want to adjust to (in %)')
                .setMinValue(1)
                .setMaxValue(client.config.opt.maxVol)
                .setRequired(true))
            .toJSON()
    }
}