module.exports = {
    app: {
        token: '***',
        playing: ' ❤️',
        global: false,
        guild: '***',
        extraMessages: false,
        loopMessage: false,
    },
    opt: {
        DJ: {
            enabled: true,
            roleName: 'DJ',
            commands: []
        },
        maxVol: 50,
        spotifyBridge: true,
        volume: 25,
        leaveOnEmpty: true,
        leaveOnEmptyCooldown: 30000,
        leaveOnEnd: true,
        leaveOnEndCooldown: 30000,
        discordPlayer: {
            ytdlOptions: {
                quality: 'highestaudio',
                highWaterMark: 1 << 25
            }
        }
    }
};
