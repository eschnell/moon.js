const BaseSubcommandExecutor = require('../../utils/BaseSubcommandExecutor')
const { EmbedBuilder } = require('discord.js')
const { JsonDB, Config } = require('node-json-db')
const { useMainPlayer, QueryType } = require('discord-player')

module.exports = class LoadSubcommand extends BaseSubcommandExecutor {

    constructor(baseCommand, group) {
        super(baseCommand, group, 'load')
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const player = useMainPlayer()
        const name = inter.options.getString('name').toLowerCase()
        var db = new JsonDB(
            new Config(`db/${inter.channel.guild.id}/playlists`, true, false, '/')
        )
        // query DB
        try {
            var data = await db.getData(`/${name}`)
        }
        catch {
            return inter
                .editReply(`Playlist ${name} doesn't exist.`)
                .then(setTimeout(() => inter.deleteReply(), 5000))
        }

        // init player on guild ID
        const queue = player.nodes.create(inter.guild, {
            np: null,
            metadata: {
                channel: inter.channel,
                np: null
            },
            spotifyBridge: client.config.opt.spotifyBridge,
            volume: client.config.opt.volume,
            leaveOnEmpty: client.config.opt.leaveOnEmpty,
            leaveOnEmptyCooldown: client.config.opt.leaveOnEmptyCooldown,
            leaveOnEnd: client.config.opt.leaveOnEnd,
            leaveOnEndCooldown: client.config.opt.leaveOnEndCooldown,
        })
        // verify voice connection
        if (!inter.member.voice.channelId) {
            const NoChannelJoined = new EmbedBuilder()
                .setAuthor({name: 'Please join a voice channel first ❌'})
                .setColor('#FF0000')
            return inter.editReply({ embeds: [NoChannelJoined] })
        }
        try {
            if (!queue.connection)
                await queue.connect(inter.member.voice.channel)
        } catch {
            await player.deleteQueue(inter.guildId)
            const NoVoiceEmbed = new EmbedBuilder()
                .setColor('#2f3136')
                .setAuthor({
                    name: `I can't join the voice channel... try again ? ❌`
                })
            return inter
                .editReply({ embeds: [NoVoiceEmbed] })
                .then(setTimeout(() => inter.deleteReply(), 5000))
        }
        // add each track one-by-one and start
        // player at the beginning if player
        // isn't running
        for (let i = 0; i < data['url'].length; i++) {
            const res = await player.search(
                data['url'][i],
                {
                    requestedBy: inter.member,
                    searchEngine: QueryType.AUTO
                })
            try { queue.addTrack(res.tracks) }
            catch {}
            if (!queue.isPlaying() && i == 1)
                await queue.node.play()
        }

        // Playlist loaded with no pb
        return inter
            .editReply({ content: `${inter.member} All good ✅` })
            .then(setTimeout(() => inter.deleteReply(), 10000))
    }
}
