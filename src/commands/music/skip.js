const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue  } = require('discord-player')

module.exports = class SkipSlashCommand extends BaseSlashCommand {
    // name: 'skip',
    // description: 'Skip the track',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('skip', 'Skip the track', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content:`No music currently playing ${inter.member}... try again ? ❌`
            })

        const success = queue.node.skip()
        const SkipEmbed = new EmbedBuilder()
            .setColor('#2f3136')
            .setAuthor({
                name:
                    success
                    ? `Current music ${queue.currentTrack.title} skipped ✅`
                    : `Something went wrong ${inter.member}... try again ? ❌`
                })

        //if (success) { global.clearProgressInterval() }
        return inter.editReply({ embeds: [SkipEmbed] })
            .then(setTimeout(() => inter.deleteReply(), 5000))
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}