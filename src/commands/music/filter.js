const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class FilterSlashCommand extends BaseSlashCommand {
    constructor() {
        super('filter', 'Add a filter to your track', 'music', true)
    }


    async run(client, inter ) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        const actualFilter = queue.filters.ffmpeg.getFiltersEnabled()[0]
        const infilter = inter.options.getString('filter')
        const filters = []

        queue.filters.ffmpeg.getFiltersEnabled().map(x => filters.push(x))
        queue.filters.ffmpeg.getFiltersDisabled().map(x => filters.push(x))
        const filter = filters.find((x) => x.toLowerCase() === infilter.toLowerCase().toString())

        if (!filter)
            return inter.editReply({
                content: `This filter doesn't exist ${inter.member}... try again ? ❌\n${actualFilter ? `Filter currently active ${actualFilter}.\n` : ''}List of available filters ${filters.map(x => `${x}`).join(', ')}.`
            })
        await queue.filters.ffmpeg.toggle(filter)
        const FilterEmbed = new EmbedBuilder()
            .setAuthor({
                name: `The filter ${filter} is now ${queue.filters.ffmpeg.isEnabled(filter) ? 'enabled' : 'disabled'} ✅\n*Reminder the longer the music is, the longer this will take.*`
            })
            .setColor('#2f3136')

       return inter.editReply({ embeds: [FilterEmbed] })
    }

    getSlashCommandJSON() {
        const choices = Object.keys(require('discord-player').AudioFilters.filters).map(m => Object({ name: m, value: m })).splice(0, 25)
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => opt
                .setName('filter')
                .setDescription('Filter you want to add')
                .setRequired(true)
                .addChoices(choices[0])
                .addChoices(choices[1])
                .addChoices(choices[2])
                .addChoices(choices[3])
                .addChoices(choices[4])
                .addChoices(choices[5])
                .addChoices(choices[6])
                .addChoices(choices[7])
                .addChoices(choices[8])
                .addChoices(choices[9])
                .addChoices(choices[10])
                .addChoices(choices[11])
                .addChoices(choices[12])
                .addChoices(choices[13])
                .addChoices(choices[14])
                .addChoices(choices[15])
                .addChoices(choices[16])
                .addChoices(choices[17])
                .addChoices(choices[18])
                .addChoices(choices[19])
                .addChoices(choices[20])
                .addChoices(choices[21])
                .addChoices(choices[22])
                .addChoices(choices[23])
                .addChoices(choices[24])
            )
            .toJSON()
    }
}