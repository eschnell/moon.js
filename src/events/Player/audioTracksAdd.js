const { EmbedBuilder } = require('discord.js')

module.exports = (queue, track) => {
    if (!client.config.app.extraMessages) return
    const audioTracksAdd = new EmbedBuilder()
        .setAuthor({name: `All the songs in playlist added into the queue ✅`})
        .setColor('#2f3136')

    queue.metadata['channel']
        .send({ embeds: [audioTracksAdd] })
        .then(msg => setTimeout(() => msg.delete(), 5000))
}