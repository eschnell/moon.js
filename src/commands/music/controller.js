const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { ApplicationCommandOptionType, ActionRowBuilder, ButtonBuilder, EmbedBuilder, PermissionsBitField, SlashCommandBuilder } = require('discord.js')

module.exports = class ControlllerSlashCommand extends BaseSlashCommand {
    // name: 'controller',
    // description: 'Set controller channel',
    // category: 'music',
    // voiceChannel: false,
    // permissions: PermissionsBitField.Flags.SendMessages,
    constructor() {
        super('controller', 'Set controller to this channel', 'music', false)
    }
    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        let Channel = inter.channel

        const embed = new EmbedBuilder()
            .setTitle('control your music from the buttons below')
            .setImage(inter.guild.iconURL({ size: 4096, dynamic: true }))
            .setColor('#2f3136')
            .setFooter({
                text: 'Music comes first - ❤️',
                iconURL: inter.member.avatarURL({ dynamic: true })
            })
        inter.editReply({ content: `sending controller to ${Channel}... ✅`})
        const back = new ButtonBuilder()
            .setLabel('Back')
            .setCustomId(JSON.stringify({ffb: 'player_back'}))
            .setStyle('Primary')
        const skip = new ButtonBuilder()
            .setLabel('Skip')
            .setCustomId(JSON.stringify({ffb: 'player_skip'}))
            .setStyle('Primary')
        const resumepause = new ButtonBuilder()
            .setLabel('Resume & Pause')
            .setCustomId(JSON.stringify({ffb: 'player_resumepause'}))
            .setStyle('Danger')
        const save = new ButtonBuilder()
            .setLabel('Save')
            .setCustomId(JSON.stringify({ffb: 'player_savetrack'}))
            .setStyle('Success')
        const volumeup = new ButtonBuilder()
            .setLabel('Volume up')
            .setCustomId(JSON.stringify({ffb: 'player_volumeup'}))
            .setStyle('Primary')
        const volumedown = new ButtonBuilder()
            .setLabel('Volume Down')
            .setCustomId(JSON.stringify({ffb: 'player_volumedown'}))
            .setStyle('Primary')
        const loop = new ButtonBuilder()
            .setLabel('Loop')
            .setCustomId(JSON.stringify({ffb: 'player_loop'}))
            .setStyle('Danger')
        const np = new ButtonBuilder()
            .setLabel('Now Playing')
            .setCustomId(JSON.stringify({ffb: 'player_nowplaying'}))
            .setStyle('Secondary')
        const queuebutton = new ButtonBuilder()
            .setLabel('Queue')
            .setCustomId(JSON.stringify({ffb: 'player_queue'}))
            .setStyle('Secondary')
        const lyrics = new ButtonBuilder()
            .setLabel('Lyrics')
            .setCustomId(JSON.stringify({ffb: 'player_lyrics'}))
            .setStyle('Primary')
        const shuffle = new ButtonBuilder()
            .setLabel('Shuffle')
            .setCustomId(JSON.stringify({ffb: 'player_shuffle'}))
            .setStyle('Success')
        const stop = new ButtonBuilder()
            .setLabel('Stop')
            .setCustomId(JSON.stringify({ffb: 'player_stop'}))
            .setStyle('Danger')
        const row1 = new ActionRowBuilder()
            .addComponents(back, queuebutton, resumepause, np, skip)
        const row2 = new ActionRowBuilder()
            .addComponents(volumedown, loop, save, volumeup)
        const row3 = new ActionRowBuilder()
            .addComponents(lyrics, shuffle, stop)

        Channel.send({ embeds: [embed], components: [row1, row2, row3] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}
