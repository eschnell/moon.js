const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class SkiptoSlashCommand extends BaseSlashCommand {
    // name: 'skipto',
    // description: "Skips to particular track in queue",
    // category: 'music',
    // voiceChannel: true,
    // options: [
    //     {
    //         name: 'song',
    //         description: 'the name/url of the track you want to skip to',
    //         type: ApplicationCommandOptionType.String,
    //         required: false,
    //     },
    //     {
    //         name: 'number',
    //         description: 'the place in the queue the song is in',
    //         type: ApplicationCommandOptionType.Number,
    //         required: false,
    //     }
    // ],
    constructor() {
        super('skipto', 'Skips to particular track in queue', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const track = inter.options.getString('song')
        const number = parseInt(inter.options.getNumber('number'))
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!track && !number)
            inter.editReply({
                content: `You have to use one of the options to jump to a song ${inter.member}... try again ? ❌`
            })
        if (track) {
            const track_skipto = queue.tracks.toArray()
                .find((t) => t.title.toLowerCase() === track.toLowerCase() || t.url === track)

            if (!track_skipto)
                return inter.editReply({
                    content: `could not find ${track} ${inter.member}... try using the url or the full name of the song ? ❌`})
            queue.node.skipTo(track_skipto)
            return inter.editReply({ content: `Jumped to ${track_skipto.title}  ✅` })
        }
        if (number) {
            const index = number - 1
            const trackname = queue.tracks.toArray()[index].title

            if (!trackname)
                return inter.editReply({
                    content: `This track dose not seem to exist ${inter.member}...  try again ?❌`
                })
            queue.node.skipTo(index)
            const skipToEmbed = new EmbedBuilder()
                .setAuthor({name: `Skiped to ${trackname} ✅`})
                .setColor('#2f3136')

            inter.editReply({ embeds: [skipToEmbed] })
        }
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addNumberOption(opt => opt
                .setName('number')
                .setDescription('The place in the queue the song is in')
                .setMinValue(1)
                .setRequired(true))
            .toJSON()
    }
}
