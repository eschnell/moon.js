const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { QueryType, useMainPlayer, useQueue } = require('discord-player')

module.exports = class PlaynextSlashCommand extends BaseSlashCommand {
    // name: 'playnext',
    // description: "Song you want to playnext",
    // category: 'music',
    // voiceChannel: true,
    // options: [
    //     {
    //         name: 'song',
    //         description: 'the song you want to playnext',
    //         type: ApplicationCommandOptionType.String,
    //         required: true,
    //     }
    // ],
    constructor() {
        super('playnext', 'Song you want to play next', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const player = useMainPlayer()
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        const song = inter.options.getString('song')
        const res = await player.search(song, {
            requestedBy: inter.member,
            searchEngine: QueryType.AUTO
        })

        if (!res || !res.tracks.length)
            return inter.editReply({
                content: `No results found ${inter.member}... try again ? ❌`
            })
        if (res.playlist)
            return inter.editReply({
                content: `This command dose not support playlist's ${inter.member}... try again ? ❌`
            })
        queue.insertTrack(res.tracks[0], 0)
        const PlayNextEmbed = new EmbedBuilder()
            .setAuthor({name: `Track has been inserted into the queue... it will play next 🎧` })
            .setColor('#2f3136')

        await inter.editReply({ embeds: [PlayNextEmbed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => opt
                .setName('song')
                .setDescription('The song you want to play next')
                .setRequired(true))
            .toJSON()
    }
}
