module.exports = { interactionMemberHasRole }

function interactionMemberHasRole(inter, roleName) {
    return inter.member._roles.includes(
        inter.guild.roles.cache.find(x =>
            x.name.toLowerCase() === roleName.toLowerCase()
        ).id
    )
}