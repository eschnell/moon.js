const ms = require('ms')
const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class SeekSlashCommand extends BaseSlashCommand {
    // name: 'seek',
    // description: 'Skip back or foward in a song',
    // category: 'music',
    // voiceChannel: true,
    // options: [
    // {
    //     name: 'time',
    //     description: 'time that you want to skip to',
    //     type: ApplicationCommandOptionType.String,
    //     required: true,
    // }
    // ],
    constructor() {
        super('seek', 'Skip back or foward in a song', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.editReply}... try again ? ❌`
            })
        const timeToMS = ms(inter.options.getString('time'))

        if (timeToMS >= queue.currentTrack.durationMS)
            return inter.editReply({
                content: `The indicated time is higher than the total time of the current song ${inter.member}... try again ? ❌\n*Try for example a valid time like **5s, 10s, 20 seconds, 1m**...*`
            })
        await queue.node.seek(timeToMS)
        const SeekEmbed = new EmbedBuilder()
            .setColor('#2f3136')
            .setAuthor({name: `Time set on the current song **${ms(timeToMS, { long: true })}** ✅`})

        inter.editReply({ embeds: [SeekEmbed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => opt
                .setName('time')
                .setDescription('Time you want to skip to')
                .setRequired(true))
            .toJSON()
    }
}