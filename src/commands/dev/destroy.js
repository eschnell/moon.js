const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { SlashCommandBuilder } = require('discord.js')
const { useMainPlayer } = require('discord-player')
const { interactionMemberIsOwner } = require('../../utils/perms/interactionMemberIsOwner')

const util = require('util')

module.exports = class DestroySlashCommand extends BaseSlashCommand {
    constructor() {
        super('destroy', 'Dev command', 'dev')
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })

        // owner check
        if (!interactionMemberIsOwner(inter))
            return inter.editReply({
                content: 'You are not allowed to use this command.'
            })

        // init
        const player = useMainPlayer()
        inter.editReply({ content: 'abc' })
    }


    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}