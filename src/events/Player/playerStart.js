const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js')

module.exports = async (queue, track) => {

    // nowPlaying Embed message
    function createEmbed(mod) {
        let node = queue.node
        var percent = node.getTimestamp()

        if (!percent)
            percent = null
        else
            percent = percent.progress
        var progress = node.createProgressBar()
        let repeatMode = queue.repeatMode
        let mode = ''
        const name = `🎧 Playing ${track.title} 🎧`
        var description = `in ${queue.channel.name}\n` +
            `__Progress:__ ${percent}%\n` +
            `${mod % 2 ? '⏳' : '⌛'} ${progress}`
        try {
            if (repeatMode > 0) {
                if (repeatMode === 1)
                    mode = 'TRACK 🔂'
                if (repeatMode === 2)
                    mode = 'QUEUE 🔁'
                description = `**>> LOOPING ${mode}**\n${description}`
            }
        } catch {
            return null
        }
        const embed = new EmbedBuilder()
            .setAuthor({ name: name, url: track.url })
            .setThumbnail(track.thumbnail)
            .setDescription(description)
            .setColor('#2f3136')
        const back = new ButtonBuilder()
            .setLabel('⏮ Back')
            .setCustomId(JSON.stringify({ ffb: 'player_back' }))
            .setStyle('Primary')
        const skip = new ButtonBuilder()
            .setLabel('⏭ Skip')
            .setCustomId(JSON.stringify({ ffb: 'player_skip' }))
            .setStyle('Primary')
        const resumepause = new ButtonBuilder()
            .setLabel('⏯ Resume/Pause')
            .setCustomId(JSON.stringify({ ffb: 'player_resumepause' }))
            .setStyle('Danger')
        const shuffle = new ButtonBuilder()
            .setLabel('🔀 Shuffle')
            .setCustomId(JSON.stringify({ ffb: 'player_shuffle' }))
            .setStyle('Secondary')
        const loop = new ButtonBuilder()
            .setLabel('Loop')
            .setCustomId(JSON.stringify({ ffb: 'player_loop' }))
            .setStyle('Secondary')
        const lyrics = new ButtonBuilder()
            .setLabel('💬🎶 Lyrics')
            .setCustomId(JSON.stringify({ ffb: 'player_lyrics' }))
            .setStyle('Secondary')
        const button_queue = new ButtonBuilder()
            .setLabel('🎶🎵 (DISABLED USE /queue FOR NOW) 🎵🎶')
            .setCustomId(JSON.stringify({ ffb: 'player_queue' }))
            .setStyle('Primary')
            .setDisabled(true)
        const row1 = new ActionRowBuilder()
            .addComponents(back, resumepause, skip)
        const row2 = new ActionRowBuilder()
            .addComponents(shuffle, loop, lyrics)
        const row3 = new ActionRowBuilder()
            .addComponents(button_queue)

        return { embeds: [embed], components: [row1, row2, row3] }
    }



    // Init
    if (!client.config.app.loopMessage)
        return
    const timer = 5000
    let ms = queue.node.getTimestamp()

    if (ms) { var now = ms.current.value, end = ms.total.value }
    else { var now = 0, end = 0 }
    var i = 0

    // Store NowPlaying message in metadata
    var np
    if ('np' in queue.metadata) {
        try { await queue.metadata['np'].then(msg => msg.delete()) }
        catch { () => {} }
    }
    np = queue.metadata['channel'].send(createEmbed(i))
    queue.setMetadata({
        channel: queue.metadata['channel'],
        np: np
    })

    const id = np.id
    try {
        var loop = null
        // Loop-edit message every `timer` ms
        loop = setInterval(() => {
            i += 1
            if (queue.currentTrack) {
                let ms
                try {
                    ms = queue.node.getTimestamp() // <==================== (?)
                } catch {
                    np = null
                    clearInterval(loop)
                }
                if (ms) { now = ms.current.value }
            } else { return }
            ////////////////////////////////////console.log(`end: ${end}\tnow: ${now}`)
            // Break if track has been skiped
            // console.log(`editable: ${np.editable}`)
            if (queue.metadata['np'] == null) {
                now = end
                return clearInterval(loop)
            }
            // Edit ProgressBar
            try {
                if (queue.metadata['np'].id === id && (end - now > timer + 5000)) {
                    try {
                        np.then(msg => {
                            if (msg.editable) { msg.edit(createEmbed(i)).catch(() => {})}
                        }
                        ).catch(() => {}) // <== ?
                    } catch { () => {} }
                }
            } catch (err) {
                //console.log(err)
                clearInterval(loop)
            }
            // Clear ticks
            if (end - now <= timer + 5000) { clearInterval(loop) }
        }, timer)
    }
    catch (err) {}
}