const path = require('path')
const fs = require('fs/promises')
const Genius = require('genius-lyrics')
const { Player } = require('discord-player')

//    src/events/Discord folder
async function loadDiscordEvents(client, filePath, folder) {
    for (const file of await fs.readdir(path.join(filePath, folder))) {
        const finalPath = path.join(filePath, folder, file)
        const event = require(finalPath)
        if (event.once) {
            client.once(event.name, (...args) => event.execute(...args))
        } else {
            client.on(event.name, async (...args) => event.execute(...args))
        }
        console.log('-> [Loaded Event]', file.split('.')[0])
        delete require.cache[require.resolve(finalPath)]
    }
}

//    src/events/Player folder
async function loadPlayerEvents(client, filePath, folder) {
    const player = new Player(client, client.config.opt.discordPlayer)
    global.genius = new Genius.Client()
    player.extractors.loadDefault()

    for (const file of await fs.readdir(path.join(filePath, folder))) {
        const finalPath = path.join(filePath, folder, file)
        const event = require(finalPath)
        player.events.on(file.split('.')[0], event.bind(null))
        console.log('-> [Loaded Event]', file.split('.')[0])
        delete require.cache[require.resolve(finalPath)]
    }
}

module.exports = {
    loadDiscordEvents,
    loadPlayerEvents
}