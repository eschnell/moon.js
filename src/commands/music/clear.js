const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class ClearSlashCommand extends BaseSlashCommand {
    constructor() {
        super('clear', 'Clear all the music in the queue', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!queue.tracks.toArray()[1])
            return inter.editReply({
                content: `No music in the queue after the current one ${inter.member}... try again ? ❌`
            })
        await queue.tracks.clear()
        const ClearEmbed = new EmbedBuilder()
            .setAuthor({name: `The queue has just been cleared 🗑️`})
            .setColor('#2f3136')

        inter.editReply({ embeds: [ClearEmbed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}