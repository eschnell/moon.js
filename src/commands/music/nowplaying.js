const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class NowplayingSlashCommand extends BaseSlashCommand {
    // name: 'nowplaying',
    // description: 'View what is playing!',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('nowplaying', 'View what is playing!', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue)
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        const track = queue.currentTrack
        const methods = ['disabled', 'track', 'queue']
        const timestamp = track.duration
        const trackDuration = timestamp.progress == 'Infinity'
            ? 'infinity (live)'
            : track.duration
        const progress = queue.node.createProgressBar()
        const now = queue.node.streamTime
        const description = `Volume **${queue.node.volume}**%\n` +
            `Duration **${trackDuration}**\nProgress ${progress}\n` +
            `Loop mode **${methods[queue.repeatMode]}**\n` +
            `Requested by ${track.requestedBy}`

        const embed = new EmbedBuilder()
            .setAuthor({
                name: track.title,
                iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true })
            })
            .setThumbnail(track.thumbnail)
            .setDescription(description)
            .setFooter({
                text: 'Music comes first - ❤️',
                iconURL: inter.member.avatarURL({ dynamic: true })
            })
            .setColor('#2f3136')
            .setTimestamp()
        const saveButton = new ButtonBuilder()
            .setLabel('Save this track')
            .setCustomId(JSON.stringify({ ffb: 'player_savetrack' }))
            .setStyle('Danger')
        const volumeup = new ButtonBuilder()
            .setLabel('Volume up')
            .setCustomId(JSON.stringify({ ffb: 'player_volumeup' }))
            .setStyle('Primary')
        const volumedown = new ButtonBuilder()
            .setLabel('Volume Down')
            .setCustomId(JSON.stringify({ ffb: 'player_volumedown' }))
            .setStyle('Primary')
        const loop = new ButtonBuilder()
            .setLabel('Loop')
            .setCustomId(JSON.stringify({ ffb: 'player_loop' }))
            .setStyle('Danger')
        const resumepause = new ButtonBuilder()
            .setLabel('Resume & Pause')
            .setCustomId(JSON.stringify({ ffb: 'player_resumepause' }))
            .setStyle('Success')
        const row = new ActionRowBuilder()
            .addComponents(volumedown, saveButton, resumepause, loop, volumeup)
        return inter.editReply({ embeds: [embed], components: [row] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}
