const { useQueue } = require('discord-player')

module.exports = async function VolumeDownButton(inter) {
    await inter.deferReply({ ephemeral: true })
    let queue = useQueue(inter.guild)
    let maxVol = client.config.opt.maxVol

    if (!queue || !queue.isPlaying())
        return inter.editReply({ content: `No music currently playing... try again ? ❌`})
    let vol = Math.floor(queue.node.volume - 5)

    if (vol < 0 )
        return inter.editReply({ content: `I can not move the volume down any more ${inter.member}... try again ? ❌`})
    if (queue.node.volume === vol)
        return inter.editReply({
            content: `The volume you want to change is already the current one ${inter.member}... try again ? ❌`
        })
    let success = queue.node.setVolume(vol)

    return inter.editReply({
        content:
            success
            ? `The volume has been modified to ${vol}/${maxVol}% 🔊`
            : `Something went wrong ${inter.member}... try again ? ❌`
    })
}