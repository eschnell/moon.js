const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class StopSlashCommand extends BaseSlashCommand {
    // name: 'stop',
    // description: 'Stop the track',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('stop', 'Stop the music bot', 'music', true)
    }

    async run(client, inter ) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying()) return inter.editReply({ content:`No music currently playing ${inter.member}... try again ? ❌` })
        queue.setMetadata({
            channel: queue.metadata['channel'],
            np: null
        })
        queue.delete()
        const StopEmbed = new EmbedBuilder()
            .setColor('#2f3136')
            .setAuthor({name: `Music stopped on this server, see you next time ✅` })

        //global.clearProgressInterval()
        return inter
            .editReply({ embeds: [StopEmbed] })
            .then(setTimeout(() => inter.deleteReply(), 5000))
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}