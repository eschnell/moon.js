const makePlaylistEmbed = require('../utils/makePlaylistEmbed')

module.exports = async function PlaylistNextButton(inter) {
    const em = inter.message.embeds[0]
    const old_page = em.footer['text'].split(' ')[1]
    await makePlaylistEmbed(inter, parseInt(old_page))
}