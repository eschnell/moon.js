const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class HistorySlashCommand extends BaseSlashCommand {
    constructor() {
        super('history', 'See the history of the queue', 'music', false)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || queue.history.tracks.toArray().length == 0)
            return inter.editReply({ content: `No music has been played yet` })
        const tracks = queue.history.tracks.toArray()
        let description = tracks
            .slice(0, 20)
            .map((track, index) => {
                return `**${index + 1}.** [${track.title}](${track.url}) by ${track.author}`
            })
            .join('\r\n\r\n')
        let HistoryEmbed = new EmbedBuilder()
            .setTitle(`History`)
            .setDescription(description)
            .setColor('#2f3136')
            .setTimestamp()
            .setFooter({
                text: 'Music comes first - ❤️',
                iconURL: inter.member.avatarURL({ dynamic: true })
            })

        inter.editReply({ embeds: [HistoryEmbed] })

    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}