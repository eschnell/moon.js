const { EmbedBuilder } = require('discord.js')

module.exports = (queue, track) => {
    if (!client.config.app.extraMessages) return
    const audioTrackAdd = new EmbedBuilder()
        .setAuthor({
            name: `Track ${track.title} added in the queue ✅`,
            iconURL: track.thumbnail
        })
        .setColor('#2f3136')

    queue.metadata['channel']
        .send({ embeds: [audioTrackAdd] })
        .then(msg => setTimeout(() => msg.delete(), 5000))
}