const { EmbedBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = async function ShuffleButton(inter) {
    await inter.deferReply({ ephemeral: true })
    let queue = useQueue(inter.guild)

    if (!queue || !queue.isPlaying())
        return inter.editReply({
            content: `No music currently playing... try again ? ❌`
        })
    if (!queue.tracks.toArray()[0])
        return inter.editReply({
            content: `No music in the queue after the current one ${inter.member}... try again ? ❌`
        })
    await queue.tracks.shuffle()
    let ShuffleEmbed = new EmbedBuilder()
        .setColor('#2f3136')
        .setAuthor({ name: `Queue shuffled ${queue.tracks.size} song(s)! ✅` })

    return inter.editReply({ embeds: [ShuffleEmbed] })
        .then(setTimeout(() => inter.deleteReply(), 5000))
}