const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class JumpSlashCommand extends BaseSlashCommand {
    constructor() {
        super('jump', 'Jumps to particular track in queue', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const number =  inter.options.getNumber('number')
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!number)
            inter.editReply({
                content: `You have to use one of the options to jump to a song ${inter.member}... try again ? ❌`
            })
        const index = number - 1
        const trackname = queue.tracks.toArray()[index].title

        if (!trackname)
            return inter.editReply({
                content: `This track dose not seem to exist ${inter.member}...  try again ?❌`
            })
        queue.node.jump(index)
        const JumpEmbed = new EmbedBuilder()
            .setAuthor({name: `Jumped to ${trackname} ✅`})
            .setColor('#2f3136')

        inter.editReply({ embeds: [JumpEmbed] })
    }

    // name: 'jump',
    // description: 'Jumps to particular track in queue',
    // category: 'music',
    // voiceChannel: true,
    // options: [
    //     {
    //         name: 'number',
    //         description: 'the place in the queue the song is in',
    //         type: ApplicationCommandOptionType.Number,
    //         required: false,
    //     }
    // ]
    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addIntegerOption(opt => opt
                .setName('number')
                .setRequired(true)
                .setDescription('The place in the queue the song is in'))
            .toJSON()
    }
}
