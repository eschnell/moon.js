const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class ResumeSlashCommand extends BaseSlashCommand  {
    // name: 'resume',
    // description: 'Play the track',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('resume', 'Play the track', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue)
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (queue.node.isPlaying())
            return inter.editReply({
                content: `The track is already running, ${inter.member}... try again ? ❌`
            })
        const success = queue.node.resume()
        const ResumeEmbed = new EmbedBuilder()
            .setAuthor({
                name: success
                ? `Current music ${queue.currentTrack.title} resumed ✅`
                : `Something went wrong ${inter.member}... try again ? ❌`
            })
            .setColor('#2f3136')
        return inter.editReply({ embeds: [ResumeEmbed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}
