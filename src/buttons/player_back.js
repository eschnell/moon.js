const { useQueue } = require('discord-player')

module.exports = async function BackButton(inter) {
    await inter.deferReply({ ephemeral: true })
    let queue = useQueue(inter.guild)

    if (!queue || !queue.isPlaying())
        return inter.editReply({
            content: `No music currently playing... try again ? ❌`
        })
    if (!queue.history.previousTrack)
        return inter.editReply({
            content: `There was no music played before ${inter.member}... try again ? ❌`
        })
    await queue.history.back()
    inter.editReply({
        content:`Playing the **previous** track ✅`
    }).then(setTimeout(() => inter.deleteReply(), 5000))
}