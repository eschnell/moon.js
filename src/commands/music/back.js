const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class BackSlashCommand extends BaseSlashCommand {
    constructor() {
        super('back', 'Go back the previous song', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.node.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!queue.history.previousTrack)
            return inter.editReply({
                content: `There was no music played before ${inter.member}... try again ? ❌`
            })
        await queue.history.back()
        const BackEmbed = new EmbedBuilder()
            .setAuthor({name: `Playing the previous track ✅`})
            .setColor('#2f3136')

        //global.clearProgressInterval()
        return inter.editReply({ embeds: [BackEmbed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}