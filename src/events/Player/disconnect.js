const { EmbedBuilder } = require('discord.js')

module.exports = (queue) => {
    const Disconnect = new EmbedBuilder()
        .setAuthor({name: `Disconnected from the voice channel, clearing the queue! ❌`})
        .setColor('#2f3136')

    try {
        //global.clearProgressInterval()
        queue.metadata['channel'].send({ embeds: [Disconnect] })
            .then(msg => setTimeout(() => msg.delete(), client.config.app.timeout * 1000))
            .catch(error => console.error(error))
    } catch (err) {
        () => {}
    }
}
