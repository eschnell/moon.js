const BaseSubcommandExecutor = require('../../utils/BaseSubcommandExecutor')

module.exports = class RemoveSubcommand extends BaseSubcommandExecutor {

    constructor(baseCommand, group) {
        super(baseCommand, group, 'remove')
    }

    async run(client, inter) {
    }
}