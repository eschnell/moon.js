const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class PauseSlashCommand extends BaseSlashCommand {
    // name: 'pause',
    // description: 'Pause the track',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('pause', 'Pause the track', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue)
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (queue.node.isPaused())
            return inter.editReply({
                content: `The track is currently paused, ${inter.member}... try again ? ❌`
            })
        const success = queue.node.setPaused(true)
        const PauseEmbed = new EmbedBuilder()
            .setAuthor({ name:
                success
                ? `Current music ${queue.currentTrack.title} paused ✅`
                : `Something went wrong ${inter.member}... try again ? ❌`
            })
            .setColor('#2f3136')

        return inter.editReply({ embeds: [PauseEmbed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}