const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class SaveSlashCommand extends BaseSlashCommand {
    // name: 'save',
    // description: 'Save the current track!',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('save', 'Save the current track!', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue)
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        inter.member.send({
            embeds: [
                new EmbedBuilder()
                    .setColor('#2f3136')
                    .setTitle(`:arrow_forward: ${queue.currentTrack.title}`)
                    .setURL(queue.currentTrack.url)
                    .addFields(
                        { name: ':hourglass: Duration:', value: `\`${queue.currentTrack.duration}\``, inline: true },
                        { name: 'Song by:', value: `\`${queue.currentTrack.author}\``, inline: true },
                        { name: 'Views :eyes:', value: `\`${Number(queue.currentTrack.views).toLocaleString()}\``, inline: true },
                        { name: 'Song URL:', value: `\`${queue.currentTrack.url}\`` }
                    )
                    .setThumbnail(queue.currentTrack.thumbnail)
                    .setFooter({
                        text:`from the server ${inter.member.guild.name}`,
                        iconURL: inter.member.guild.iconURL({ dynamic: false })
                    })
            ]
        }).then(() => {
            return inter.editReply({
                content: `I have sent you the title of the music by private messages ✅`
            })
        }).catch(error => {
            return inter.editReply({
                content: `Unable to send you a private message... try again ? ❌`
            })
        })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}