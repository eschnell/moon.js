const BaseSubcommandExecutor = require('../../utils/BaseSubcommandExecutor')
const { JsonDB, Config } = require('node-json-db')
const { EmbedBuilder } = require('discord.js')
const { useQueue } = require('discord-player')
const { setTimeout } = require('timers/promises')


module.exports = class SaveSubcommand extends BaseSubcommandExecutor {
	constructor(baseCommand, group) {
		super(baseCommand, group, 'save')
	}

    

	async run(client, inter) {

        function errorEmbed(str) {
            return new EmbedBuilder()
                .setColor('#FF0000')
                .setAuthor({name: str})
        }

		await inter.deferReply({ ephemeral: true })
        // DB obj
        var db = new JsonDB(
            new Config(`db/${inter.channel.guild.id}/playlists`, true, false, '/')
        )
        const name = inter.options.getString('name').toLowerCase()
        //const queue = new useQueue(inter.guild)
        const queue = useQueue(inter.guild)
        // check if user is in a channel
        if (!inter.member.voice.channelId) {
            return inter.editReply({
                embeds: [
                    errorEmbed('Please join a voice channel first ❌')
                ]
            }).then(setTimeout(() => inter.deleteReply(), 10000))
        }
        //console.log(queue, typeof(queue))

        // check any whitespaces
        if ( !/^\w+$/.test(name) )
            return inter.editReply({
                embeds: [
                    errorEmbed('Playlist name can not contain any spaces nor illegal chars ❌')
                ]
            }).then(setTimeout(() => inter.deleteReply(), 10000))

        // check if there is an existing queue
        if (!queue)
            return inter.editReply({
                embeds: [ errorEmbed(`No music currently in queue... ❌`)
                    //.catch(() => {})
                ]
            }).then(setTimeout(() => inter.deleteReply(), 10000))


        // Data to keep
        const dict = {
            //person saving the playlist
            createdBy: inter.user.username,
            //array of url
            url: queue.tracks
                .toArray()
                .map((track) => `${track.url}`)
        }
        dict['url'] = [queue.currentTrack.url].concat(dict['url'])
        // Query DB
        try {
            var data = await db.getData(`/${name}`)
        }
        catch {}
        // save Playlist after checks
        const chan = inter.channel
        if (data) {
            // Display confirmation dialog for already existing playlists in
            // case of accidental overwrites
            var replied = false
            const answers = ['y', 'yes', 'n', 'no']
            // filter messages by author interaction and only if
            // message replied is part of `answers`
            let filter = (m) =>
                m.author.id === inter.member.id &&
                answers.includes(m.content.toLowerCase())
            const confirmation = await chan
                .send(`${inter.member} ` +
                    `Playlist **${name}** already exists. ` +
                    `Do you wish to overwrite it? ( \`yes\` /  \`no\` )`
                )
            const timer = 30_000
            const collector = await confirmation.channel.createMessageCollector({ filter,
                max: 1,
                time: timer,
                idle: timer
            })
            // wait for response from user input
            collector.once('collect', async (m) => {
                try {
                    // NO
                    if (m.content.toLowerCase() === answers[2] || m.content.toLowerCase() === answers[3]) {
                        replied = true
                        chan.send(`Playlist was not saved.`)
                    }
                    // YES ( save Playlist on user confirm )
                    if (m.content.toLowerCase() === answers[0] || m.content.toLowerCase() === answers[1]) {
                        replied = true
                        db.push(`/${name}`, dict, true)
                        chan.send(`Playlist saved as \`${name}\`!`)
                    }
                } catch { () => {} }
                inter.deleteReply()
            })
            await setTimeout(timer)     // SHORT-CIRCUITED
            await inter.deleteReply().catch(() => {})
            if (replied)
                return
            //console.log(`\ncollector data:`, collector)
            return await chan.send({ content: 'No user input, playlist was not saved.' })
        }
        // save playlist if doesn't exist
        await db.push(`/${name}`, dict, true)
        await inter.deleteReply()
        await chan.send(`Playlist saved as \`${name}\`!`)
	}
}