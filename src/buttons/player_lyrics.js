const { EmbedBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = async function LyricsButton(inter) {
    await inter.deferReply({ ephemeral: true })
    let queue = useQueue(inter.guild)

    // search lyrics if possible
    if (!queue || !queue.isPlaying())
        return inter.editReply({ content: `No music currently playing... try again ? ❌`})
    try {
        let search = await genius.songs.search(
            `${queue.currentTrack.title} ${queue.currentTrack.author}`
        )
        let song = search.find(
            song => song.artist.name.toLowerCase() === queue.currentTrack.author.toLowerCase()
        )
        // failover
        if (!song)
            return inter.editReply({
                content: `No lyrics found for ${queue.currentTrack.title}... try again ? ❌`
            })
        // split embeds by 4096 chars max
        let lyrics = await song.lyrics()
        let embeds = []
        for (let i = 0; i < lyrics.length; i += 4096) {
            const toSend = lyrics.substring(i, Math.min(lyrics.length, i + 4096))
            embeds.push(new EmbedBuilder()
                .setTitle(`Lyrics for ${queue.currentTrack.title}`)
                .setDescription(toSend)
                .setColor('#2f3136')
                .setTimestamp()
                .setFooter({
                    text: 'Music comes first - ❤️',
                    iconURL: inter.member.avatarURL({ dynamic: true })
                })
            )
        }
        return inter.editReply({ embeds: embeds})
    } catch (error) {
        inter.editReply({ content: `Error! Please contact Developers! | ❌`})
        console.trace(`${error}\n`)
    }
}