const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { SlashCommandBuilder } = require('discord.js')
const { useMainPlayer } = require('discord-player')
const { interactionMemberIsOwner } = require('../../utils/perms/interactionMemberIsOwner')

const util = require('util')

module.exports = class OnlinePlayersSlashCommand extends BaseSlashCommand {
    constructor() {
        super('onlineplayers', 'List all online players (all guilds)', 'dev')
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })

        // owner check
        if (!interactionMemberIsOwner(inter))
            return inter.editReply({
                content: 'You are not allowed to use this command.'
            })

        // init
        const player = useMainPlayer()
        var running_str = ''
        var i = 0
        player.nodes.cache.forEach(node => {
            running_str +=
                `\n${node.options.guild.id}\t==> ${node.options.guild.name}`
            i += 1
        })
        if (running_str == '')
            return inter.editReply({ content: 'Nothing playing!' })
        running_str = `Currently ${i} running music player(s)${running_str}`
        inter.editReply({ content: running_str })
    }


    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}