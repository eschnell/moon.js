# syntax=docker/dockerfile:1

# Comments are provided throughout this file to help you get started.
# If you need more help, visit the Dockerfile reference guide at
# https://docs.docker.com/go/dockerfile-reference/

# Want to help us make this template better? Share your feedback here: https://forms.gle/ybq9Krt8jtBL3iCk7

ARG NODE_VERSION=19.7.0

# 1/7
# Image pull
FROM debian:bookworm-slim
#FROM node:${NODE_VERSION}-alpine

# Use production node environment by default.
ENV NODE_ENV testing
# Path to project source
#ENV PATH /home/raging/moon.js

# 2/7
# Set working env folder
WORKDIR /build

# 3/7
# Environment caching
RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked    \
    --mount=target=/var/cache/apt,type=cache,sharing=locked        \
    rm -f /etc/apt/apt.conf.d/docker-clean                         \
    && apt-get update                                              \
    && apt-get -y --no-install-recommends install                  \
        ruby ruby-dev gcc
# Source caching
RUN --mount=type=bind,source=src,target=/build/src

# 4/7
# Pull in dependencies
RUN apt-get update && apt-get -y install python3 nodejs npm ffmpeg
# --no-install-recommends --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Download dependencies as a separate step to take advantage of Docker's caching.
# Leverage a cache mount to /root/.npm to speed up subsequent builds.
# Leverage a bind mounts to package.json and package-lock.json to avoid having to copy them into
# into this layer.
# RUN --mount=type=bind,source=package.json,target=package.json \
#     --mount=type=bind,source=package-lock.json,target=package-lock.json \
#     --mount=type=cache,target=/root/.npm \
#     npm ci --omit=dev


# 5/7
# Add user node
#RUN useradd -m -d /build node
# Run the application as a non-root user.
#USER node


# 6/7
# Copy the rest of the source files into the image.
COPY . /build
# Chown
#RUN chown -R node /build


# 7/7
# Install dependencies
RUN npm install

# Run the application.
CMD node --env-file ./.env src/index.js