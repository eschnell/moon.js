module.exports = { interactionMemberIsOwner }

function interactionMemberIsOwner(inter) {
    return client.config.app.owner === inter.member.id
}