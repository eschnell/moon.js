const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { QueryType, useMainPlayer } = require('discord-player')

module.exports = class PlaySlashCommand extends BaseSlashCommand {
    constructor() {
        super('play', 'Play song or playlist!', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const player = useMainPlayer()
        const song = inter.options.getString('song_or_url')
        const res = await player.search(song, {
            requestedBy: inter.member,
            searchEngine: QueryType.AUTO
        })
        const NoResultsEmbed = new EmbedBuilder()
            .setAuthor({ name: `No results found... Make sure the playlist isn't private and try again ? ❌` })
            .setColor('#FF0000')

        if (!res || !res.tracks.length)
            return inter.editReply({ embeds: [NoResultsEmbed] })
        const queue = await player.nodes.create(inter.guild, {
            np: null,
            metadata: {
                channel: inter.channel,
                np: null
            },
            spotifyBridge: client.config.opt.spotifyBridge,
            volume: client.config.opt.volume,
            repeatMode: -1,
            leaveOnEmpty: client.config.opt.leaveOnEmpty,
            leaveOnEmptyCooldown: client.config.opt.leaveOnEmptyCooldown,
            leaveOnEnd: client.config.opt.leaveOnEnd,
            leaveOnEndCooldown: client.config.opt.leaveOnEndCooldown,
        })

        if (!inter.member.voice.channelId) {
            const NoChannelJoined = new EmbedBuilder()
                .setAuthor({name: 'Please join a voice channel first ❌'})
                .setColor('#FF0000')
            return inter.editReply({ embeds: [NoChannelJoined] })
        }
        try {
            if (!queue.connection)
                await queue.connect(inter.member.voice.channel)
        } catch {
            await player.deleteQueue(inter.guildId)
            const NoVoiceEmbed = new EmbedBuilder()
                .setAuthor({ name: 'I can\'t join the voice channel... try again ? ❌'})
                .setColor('#FF0000')
            return inter.editReply({ embeds: [NoVoiceEmbed] })
        }
        const playEmbed = new EmbedBuilder()
            .setAuthor({
                name: `Loading your ${res.playlist ? 'playlist' : 'track'} to the queue... ✅`
            })
            .setColor('#2f3136')

        await inter.editReply({ embeds: [playEmbed] })
            .then(setTimeout(() => inter.deleteReply(), client.config.app.timeout * 1000 / 2))
        res.playlist
        ? queue.addTrack(res.tracks)
        : queue.addTrack(res.tracks[0])
        if (!queue.isPlaying())
            await queue.node.play()
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => opt
                .setName('song_or_url')
                .setDescription('The song(s) you want to play (link / keywords)')
                .setRequired(true))
            .toJSON()
    }
}