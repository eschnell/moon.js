//const { Util } = require('discord-player')
const { EmbedBuilder, ButtonBuilder, ActionRowBuilder } = require('discord.js')
const { JsonDB, Config } = require('node-json-db')
const { setTimeout } = require('timers/promises')

//var util = require('util')

module.exports = async function makePlaylistEmbed(inter, page) {

    function populate_pages(data) {
        const keys = Object.keys(data)
        var len = keys.length
        const max_pages = len / 25
        new_page = page + 1

        const PlaylistEmbed = new EmbedBuilder()
            .setDescription('Saved playlists:')
            .setFooter({text: `Page ${new_page} of ${Math.ceil(max_pages)}`})

        for (let i = 25 * page; (i < 25 * (new_page)) && (i < len); i++) {
            const author = data[`${keys[i]}`]['createdBy']
            const songs = data[`${keys[i]}`]['url']
            PlaylistEmbed.addFields({
                name: `__${keys[i]}__`,
                inline: true,
                value: `\`\`\`ansi\n[2;41m[2;32m[0m[2;41m[0m[2;32m[0m[2;40m[2;31m[0m[2;40m[0m[2;40m[2;31m${songs.length} [0m[2;40msongs\n[0mAuthor: [2;33m${author}[0m\`\`\``
            })
        }
        return [PlaylistEmbed]
    }

    // START <================================================
    await inter.deferReply()
    // get DB object
    var db = new JsonDB(new Config(`db/${inter.channel.guild.id}/playlists`, true, false, '/'))
    var data = await db.getData('/')
    const author = await db.getData('/')['createdBy']
    //console.log(`data: ${data}\n`)
    let length = Object.keys(data).length
    const max_pages = Math.floor(length / 25)

    //console.log (`page:`,page) //
    //console.log (`max_page: ${max_pages}\n`) //
    // No playlist
    if (!length) {
        const NoPlaylistEmbed = new EmbedBuilder()
            .setAuthor({ name: `No playlist saved for ${inter.channel.guild.name} ❌` })
            .setColor('#2f3136')
        inter.editReply({ embeds: [NoPlaylistEmbed] })
        await setTimeout(15000),
        await inter.deleteReply()
        return
    } else {
        // Less than 25 playlists
        if (length < 25) {
            inter.editReply({ embeds: populate_pages(data) })
        // More than 25 playlists
        } else {
            const first = new ButtonBuilder()
                .setLabel('⏮')
                .setCustomId(JSON.stringify({ffb: 'playlist_first'}))
                .setStyle('Primary')
            const previous = new ButtonBuilder()
                .setLabel('Previous page')
                .setCustomId(JSON.stringify({ffb: 'playlist_previous'}))
                .setStyle('Secondary')
            if (!page) {
                first.setDisabled(true)
                previous.setDisabled(true)
            }
            const next = new ButtonBuilder()
                .setLabel('Next page')
                .setCustomId(JSON.stringify({ffb: 'playlist_next'}))
                .setStyle('Secondary')
            const last = new ButtonBuilder()
                .setLabel('⏭')
                .setCustomId(JSON.stringify({ffb: 'playlist_last'}))
                .setStyle('Primary')
            if (page === max_pages) {
                    last.setDisabled(true)
                    next.setDisabled(true)
                }
            var row1 = new ActionRowBuilder().addComponents(first, previous, next, last)
            //console.log(util.inspect(inter))
            inter.editReply({ embeds: populate_pages(data), components: [row1] })
        }
        await setTimeout(60000)
        await inter.deleteReply()
    }
}