# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.3](https://gitlab.com/eschnell/moon.js/compare/v1.0.2...v1.0.3) (2024-06-26)

### [1.0.2](https://gitlab.com/eschnell/moon.js/compare/v1.0.1...v1.0.2) (2024-06-26)

### [1.0.1](https://gitlab.com/eschnell/moon.js/compare/v0.3.15...v1.0.1) (2024-05-26)

### [0.3.15](https://gitlab.com/eschnell/moon.js/compare/v0.3.14...v0.3.15) (2024-05-26)


### Features

* **debug:** debug toggle flag ([aa0862b](https://gitlab.com/eschnell/moon.js/commit/aa0862b23d49a05cca3fc9ed864efbfb1cd2e406))

### [0.3.14](https://gitlab.com/eschnell/moon.js/compare/v0.3.13...v0.3.14) (2024-05-26)

### [0.3.13](https://gitlab.com/eschnell/moon.js/compare/v0.3.12...v0.3.13) (2024-05-26)


### Bug Fixes

* fix unhandled crash on playlist load whilst outside of voice channel ([58742bf](https://gitlab.com/eschnell/moon.js/commit/58742bf364dfb20e294c8adfb0a8f79ed22a4bf1))

### [0.3.12](https://gitlab.com/eschnell/moon.js/compare/v0.3.11...v0.3.12) (2024-05-19)


### Features

* new dev commands ([8a3ca1b](https://gitlab.com/eschnell/moon.js/commit/8a3ca1bb14ca705e8978aa461b35e284352d65bc))


### Bug Fixes

* **permissions:** major perms check ([05333d4](https://gitlab.com/eschnell/moon.js/commit/05333d4a9647b74a91fe76ca5080202b9ef1c1de))

### [0.3.11](https://gitlab.com/eschnell/moon.js/compare/v0.3.10...v0.3.11) (2024-04-28)


### Bug Fixes

* **save.js:** wip on playing saving logic ([257d833](https://gitlab.com/eschnell/moon.js/commit/257d833e3edeb9c58f430b3ba4f59624c4c0e23d))

### [0.3.10](https://gitlab.com/eschnell/moon.js/compare/v0.3.9...v0.3.10) (2024-04-07)


### Features

* permissions support for both commands and buttons :) ([cad7e69](https://gitlab.com/eschnell/moon.js/commit/cad7e6909934cf279930c3f1412f2ed2634d6ec1))

### [0.3.9](https://gitlab.com/eschnell/moon.js/compare/v0.3.8...v0.3.9) (2024-04-07)

### [0.3.8](https://gitlab.com/eschnell/moon.js/compare/v0.3.7...v0.3.8) (2024-04-07)


### Features

* added permissions support ([33e578f](https://gitlab.com/eschnell/moon.js/commit/33e578f7d3fbeb197dcf25612ee509a8548c1b28))

### [0.3.7](https://gitlab.com/eschnell/moon.js/compare/v0.3.6...v0.3.7) (2024-04-07)

### [0.3.6](https://gitlab.com/eschnell/moon.js/compare/v0.3.5...v0.3.6) (2024-04-07)


### Bug Fixes

* exception fix on `playlist save` ([6597c2f](https://gitlab.com/eschnell/moon.js/commit/6597c2f803a797a5da4c8a1b71a1ab203e90a2f7))
* queue fixed undefined property ([6258764](https://gitlab.com/eschnell/moon.js/commit/625876472f410d66d01747cb5db584d4a6a7cd52))
* resume/pause feedback hotfix ([bb3e87c](https://gitlab.com/eschnell/moon.js/commit/bb3e87cf90bec99d15fd1f3cfccb862f4511649e))

### [0.3.5](https://gitlab.com/eschnell/moon.js/compare/v0.3.4...v0.3.5) (2024-04-06)

### [0.3.4](https://gitlab.com/eschnell/moon.js/compare/v0.3.3...v0.3.4) (2024-04-06)

### 0.3.3 (2024-04-06)
