const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { SlashCommandBuilder, EmbedBuilder } = require('discord.js')

module.exports = class LeaveSlashCommand extends BaseSlashCommand {
    constructor() {
        super('leave', 'leave command', 'moderation')
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        if (client.config.app.owner != inter.member.id)
            return inter.editReply({
                embeds: [ new EmbedBuilder()
                    .setColor('#ff0000')
                    .setDescription(`❌ | You need do not have the proper permissions to execute this command`)]
            })
        //console.log(inter.member.guild)
        inter.editReply({ content: 'Leaving guild...' })
        await inter.member.guild.leave()
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
        .setName(this.name)
        .setDescription(this.description)
        .toJSON()
    }
}
