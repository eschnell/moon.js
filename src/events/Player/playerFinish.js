const { EmbedBuilder } = require('discord.js')

module.exports = async (queue, track) => {
    let repeatMode = queue.repeatMode
    // console.log(`repeat_mode: ${repeatMode}`)
    // console.log(`queue_len: ${queue.tracks.size}`)

    try {
        // End player
        if (repeatMode !== 1 && repeatMode !== 2 && !queue.tracks.size) {
            const emptyQueue = new EmbedBuilder()
                .setAuthor({name: `No more songs in the queue! ❌`})
                .setColor('#2f3136')
            await queue.metadata['np'].then(msg => msg.delete())
            queue.metadata['channel'].send({ embeds: [emptyQueue] })
                .then(msg => setTimeout(() =>
                    msg.delete(), client.config.app.timeout * 1000)
                )
                .catch(error => console.error(error))
            await new Promise(r => setTimeout(r, 10000))
            queue.delete()
        }
        // Loop track
        if (repeatMode === 1) {
            await queue.tracks.clear()
        }
        // Loop queue
        if (repeatMode === 2) {
            if (queue.tracks.size) {
                await queue.insertTrack(track, queue.tracks.size - 1)
            }
        }
    } catch {
        () => {}
    }
}