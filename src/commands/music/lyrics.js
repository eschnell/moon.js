const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class LyricsSlashCommand extends BaseSlashCommand {
    // name: 'lyrics',
    // description: 'Get lyrics for the current track',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('lyrics', 'Get lyrics for the current track', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        try {
            const search = await genius.songs.search(queue.currentTrack.title)
            const song = search.find(song => song.artist.name.toLowerCase() === queue.currentTrack.author.toLowerCase())

            if (!song)
                return inter.editReply({
                    content: `No lyrics found for ${queue.currentTrack.title}... try again ? ❌`
                })
            const lyrics = await song.lyrics()
            const embeds = []

            for (let i = 0; i < lyrics.length; i += 4096) {
                // split embeds by 4096 chars max
                const toSend = lyrics.substring(i, Math.min(lyrics.length, i + 4096))
                embeds.push(new EmbedBuilder()
                    .setTitle(`Lyrics for ${queue.currentTrack.title}`)
                    .setDescription(toSend)
                    .setColor('#2f3136')
                    .setTimestamp()
                    .setFooter({
                        text: 'Music comes first - ❤️',
                        iconURL: inter.member.avatarURL({ dynamic: true })
                    })
                )
            }
            return inter.editReply({ embeds: embeds })
        } catch (error) {
            inter.editReply({ content: `Error! Please contact Developers! | ❌`})
        }
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}

