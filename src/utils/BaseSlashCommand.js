module.exports = class BaseSlashCommand {
    constructor(name, description, category, vc) {
        this._name = name
        this._description = description
        this._category = category
        this._vc = vc
    }

    get name() {
        return this._name
    }
    get description() {
        return this._description
    }
    get category() {
        return this._category
    }
    get vc() {
        return this._vc
    }
}
