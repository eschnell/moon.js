const { useQueue } = require('discord-player')

module.exports = async function ResumePauseButton(inter) {
    await inter.deferReply({ ephemeral: true })
    const queue = useQueue(inter.guild)

    if (!queue || !queue.isPlaying())
        return inter.editReply({
            content: `No music currently playing... try again ? ❌`
        })
    const resumed = queue.node.resume()
    let message = `Current music ${queue.currentTrack.title} resumed ✅`

    if (!resumed) {
        queue.node.pause()
        message = `Current music ${queue.currentTrack.title} paused ✅`
    }
    inter
        .editReply({ content: message })
        .then(() => setTimeout(() => inter.deleteReply(), 5000))
}