const { EmbedBuilder } = require('discord.js')

module.exports = async (queue, track) => {
    const playerSkip = new EmbedBuilder()
        .setAuthor({
            name: `Skipping \`${track.title}\` ✅`,
            iconURL: track.thumbnail}
        )
        .setColor('#EE4B2B')
    const chan = queue.metadata['channel']

    // Delete NP message on track skip
    try {
        await queue.metadata['np'].then(msg => msg.delete())
    } catch {}
    queue.setMetadata({
        channel: chan,
        np: null
    })
    chan.send({ embeds: [playerSkip] }).then(msg =>
        setTimeout(() => msg.delete(), 5000)
    )
}
