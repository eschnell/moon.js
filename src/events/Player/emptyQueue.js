const { EmbedBuilder } = require('discord.js')

module.exports = (queue) => {
    const emptyQueue = new EmbedBuilder()
        .setAuthor({name: `No more songs in the queue! ❌`})
        .setColor('#2f3136')

    try {
        queue.metadata['channel'].send({ embeds: [emptyQueue] })
            .then(msg =>
                setTimeout(() => msg.delete(), client.config.app.timeout * 1000)
            )
            .catch(error => console.error(error))
    } catch (err) {}
}