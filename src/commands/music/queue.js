const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class QueueSlashCommand extends BaseSlashCommand {
    // name: 'queue',
    // description: 'Get the songs in the queue',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('queue', 'Get the songs in the queue', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = new useQueue(inter.guild)
        const time = queue.durationFormatted

        if (!queue)
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!queue.tracks)
            return inter.editReply({
                content: `No music in the queue after the current one ${inter.member}... try again ? ❌`
            })
        const methods = ['', '🔁', '🔂']
        const songs = queue.tracks.size
        const nextSongs =
            songs > 5
            ? `And **${songs - 5}** other song(s)...`
            : `In the playlist **${songs}** song(s)...`
        const tracks = queue.tracks.map((track, i) => `**${i + 1}** - ${track.title} | ${track.author} (requested by : ${track.requestedBy.username})`)
        const description = `Playlist duration: **${time}**\n` +
            `Current: ${queue.currentTrack.title}\n\n` +
            `${tracks.slice(0, 5).join('\n')}\n\n${nextSongs}`
        const embed = new EmbedBuilder()
            .setColor('#2f3136')
            .setThumbnail(inter.guild.iconURL({ size: 2048, dynamic: true }))
            .setAuthor({
                name: `Server queue - ${inter.guild.name} ${methods[queue.repeatMode]}`,
                iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true })
            })
            .setDescription(description)
            .setTimestamp()
            .setFooter({
                text: 'Music comes first ❤️ -',
                iconURL: inter.member.avatarURL({ dynamic: true })
            })

        return inter.editReply({ embeds: [embed] })
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}