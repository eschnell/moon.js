const { InteractionType, EmbedBuilder } = require('discord.js')
const { registerButtons } = require('../../utils/registry')
const { interactionMemberHasRole } = require('../../utils/perms/interactionMemberHasRole')

const util = require('util')

module.exports = {
    name: 'interactionCreate',
    async execute(interaction) {
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // V0.3                                                                   //
    ////////////////////////////////////////////////////////////////////////////
        const DJ = client.config.opt.DJ
        const owner = (client.config.app.owner == interaction.member.id)
        const embed = new EmbedBuilder().setColor('#ff0000')
        var command = client.slashCommands.get(interaction.commandName)
        if (!command)
            command = client.slashSubcommands.get(interaction.commandName)

        if (!interaction.guild.roles.cache.find(x =>
            x.name.toLowerCase() === DJ.roleName.toLowerCase())) {
            // If role `dj` doesn't exist in guild
            await interaction.deferReply({ ephemeral: true })
            return interaction
                .editReply({
                    embeds: [
                        embed.setDescription(`❌ | Role \`${DJ.roleName}\` does not exist !`)
                    ]
                })
                .then(() => setTimeout(() => interaction.deleteReply(), 10000))
        }

        if (command) {
            if ( !owner &&
                DJ.enabled &&
                DJ.commands.includes(command['name']) &&
                !interactionMemberHasRole(interaction, DJ.roleName) ) {
                    // If user doesn't have the `dj` role
                    await interaction.deferReply({ ephemeral: true })
                    return interaction
                        .editReply({
                            embeds: [
                                embed.setDescription(`❌ | You do not have the \`${DJ.roleName}\` role`)
                            ]
                        })
                        .then(() => setTimeout(() => interaction.deleteReply(), 10000))
                }
        } // end of `if (cmd)`

        ////////////////////////////////////////////

        if (interaction.isChatInputCommand()) {
            // For subcommands where the `command` object doesn't exist
            const { commandName } = interaction
            var cmd = client.slashCommands.get(commandName)
            const subcommandGroup = interaction.options.getSubcommandGroup(false)
            const subcommandName = interaction.options.getSubcommand(false)

            // Subcommands handling
            if (subcommandName) {
                cmd = client.slashSubcommands.get(commandName)
                if (subcommandGroup) {
                    // have subfolders
                    const subcommandInstance = client.slashSubcommands.get(commandName)
                    subcommandInstance.groupCommands
                        .get(subcommandGroup)
                        .get(subcommandName)
                        .run(client, interaction)
                } else {
                    // no subfolder
                    const subcommandInstance = client.slashSubcommands.get(commandName)
                    try {
                    subcommandInstance.groupCommands
                        .get(subcommandName)
                        .run(client, interaction)
                    } catch (err) {
                        console.error(err)
                    }
                }
                return
            }
            if (cmd)
                cmd.run(client, interaction).catch((err) => console.log(err))
            else
                interaction
                    .reply({ content: 'This command has no run method.' })
                    .catch((err) => {console.log(err)})
        }
        // Hook BUTTONS
        if (interaction.type === InteractionType.MessageComponent) {
            const buttonId = JSON.parse(interaction.customId)
            if ( !owner &&
                DJ.enabled &&
                DJ.buttons.includes(buttonId['ffb']) &&
                !interactionMemberHasRole(interaction, DJ.roleName)) {
                    // If user doesn't have the `dj` role
                    await interaction.deferReply({ ephemeral: true })
                    return interaction
                        .editReply({
                            embeds: [
                                embed.setDescription(`❌ | You do not have the \`${DJ.roleName}\` role`)
                            ]
                        })
                        .then(() => setTimeout(() => interaction.deleteReply(), 10000))
                }
            // execute button from src/buttons/
            registerButtons(client, interaction)
        }
    }
}