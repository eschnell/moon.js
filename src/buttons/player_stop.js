const { EmbedBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = async function StopButton(inter) {
    await inter.deferReply({ ephemeral: true })
    let queue = useQueue(inter.guild)

    if (!queue || !queue.isPlaying())
        return inter.editReply({ content: `No music currently playing... try again ? ❌`})
    queue.delete()
    let StopEmbed = new EmbedBuilder()
        .setColor('#2f3136')
        .setAuthor({name: `Music stopped into this server, see you next time ✅` })
    return inter.editReply({ embeds: [StopEmbed]})
}