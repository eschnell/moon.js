const { EmbedBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = async function NowPlayingButton(inter) {
    await inter.deferReply({ ephemeral: true })
    const queue = useQueue(inter.guild)

    if (!queue || !queue.isPlaying())
        return inter.editReply({
            content: `No music currently playing... try again ? ❌`
        })
    const track = queue.currentTrack
    const methods = ['disabled', 'track', 'queue']
    const timestamp = track.duration
    const trackDuration =
        timestamp.progress == 'Infinity'
        ? 'infinity (live)'
        : track.duration
    const progress = queue.node.createProgressBar()
    const description = `Volume **${queue.node.volume}**%\n` +
        `Duration **${trackDuration}**\n` +
        `Progress ${progress}\n` +
        `Loop mode **${methods[queue.repeatMode]}**\n` +
        `Requested by ${track.requestedBy}`
    const embed = new EmbedBuilder()
        .setAuthor({
            name: track.title,
            iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true })
        })
        .setThumbnail(track.thumbnail)
        .setDescription(description)
        .setFooter({
            text: 'Music comes first - ❤️',
            iconURL: inter.member.avatarURL({ dynamic: true })
        })
        .setColor('ff0000')
        .setTimestamp()

    inter.editReply({ embeds: [embed]})
}
