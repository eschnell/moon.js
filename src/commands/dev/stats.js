const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder, embedLength } = require('discord.js')
const { useMainPlayer } = require('discord-player')
const { interactionMemberIsOwner } = require('../../utils/perms/interactionMemberIsOwner')

const util = require('util')

module.exports = class StatsSlashCommand extends BaseSlashCommand {
    constructor() {
        super('stats', 'Dev command', 'dev')
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })

        // owner check
        if (!interactionMemberIsOwner(inter))
            return inter.editReply({
                content: 'You are not allowed to use this command.'
            })

        // uptime
        String.prototype.toHHMMSS = function() {
            var sec_num = parseInt(this, 10)
            var hours = Math.floor(sec_num / 3600)
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60)
            var seconds = sec_num - (hours * 3600) - (minutes * 60)

            if (hours < 10) { hours = '0' + hours }
            if (minutes < 10) { minutes = '0'+ minutes }
            if (seconds < 10) { seconds = '0'+ seconds }
            var time = hours + ':' + minutes + ':' + seconds
            return time
        }
        function getUptime() {
            var time = process.uptime()
            var uptime = (time + '').toHHMMSS()
            return uptime
        }
        // embed
        const StatsEmbed = new EmbedBuilder()
            .setDescription('About Moon.js')
            //.setFooter({text: `--`})
        StatsEmbed.addFields({
            name: '📈⏳ __Uptime__',
            inline: true,
            value: `\`\`\`ansi\n[2;34m[2;40m(in hour:min:sec)\n[0m[2;34m[0m[0;2m${getUptime()}[0m\`\`\``
        })
        inter.editReply({ embeds: [StatsEmbed] })
    }


    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}