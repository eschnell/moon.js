const path = require('path')
const fs = require('fs/promises')
const { Collection } = require('discord.js')
const { loadDiscordEvents, loadPlayerEvents } = require('./loadEvents')

//var util = require('util')

async function registerCommands(client, dir = '../commands') {
    const filePath = path.join(__dirname, dir)
    const files = await fs.readdir(filePath)

    for (const file of files) {
        const stat = await fs.lstat(path.join(filePath, file))
        if (stat.isDirectory())
            await registerCommands(client, path.join(dir, file))
        if (file.endsWith('.js')) {
            const Command = require(path.join(filePath, file))
            const cmd = new Command()
            client.slashCommands.set(cmd.name, cmd)
            //console.log(`===========\nCACHE :\n${util.inspect(client.slashCommands)}\n===================`)
            delete require.cache[require.resolve(path.join(filePath, file))]
            console.log('-> [Loaded Command]', cmd.name)
        }
    }
}

async function registerSubcommands(client, dir = '../subcommands') {
    const filePath = path.join(__dirname, dir)
    const files = await fs.readdir(filePath)
    //
    for (const file of files) {
        // exclude all slashcommands except `playlist`
        if (file != 'playlist')
            continue
        //console.log(file)
        const stat = await fs.lstat(path.join(filePath, file))
        if (stat.isDirectory()) {
            const subcommandDirectoryFiles = await fs.readdir(
                path.join(filePath, file)
            )
            const indexFilePos = subcommandDirectoryFiles.indexOf('index.js')
            subcommandDirectoryFiles.splice(indexFilePos, 1)
            try {
                const BaseSubcommand = require(path.join(filePath, file, 'index.js'))
                const subcommand = new BaseSubcommand()
                client.slashSubcommands.set(file, subcommand)
                for (const group of subcommand.groups) {
                    for (const command of group.subcommands) {
                        const SubcommandClass = require(path.join(
                            filePath,
                            file,
                            group.name,
                            command
                        ))
                        let subcommandGroupMap = subcommand.groupCommands.get(group.name)
                        if (subcommandGroupMap) {
                            subcommandGroupMap.set(
                                command,
                                new SubcommandClass(file, group.name, command)
                            )
                        } else {
                            subcommandGroupMap = new Collection()
                            subcommandGroupMap.set(
                                command,
                                new SubcommandClass(file, group.name, command)
                            )
                        }
                        subcommand.groupCommands.set(group.name, subcommandGroupMap)
                    }
                    const fileIndex = subcommandDirectoryFiles.indexOf(group.name)
                    subcommandDirectoryFiles.splice(fileIndex, 1)
                }
                for (const subcommandFile of subcommandDirectoryFiles) {
                    const Subcommand = require(path.join(filePath, file, subcommandFile))
                    const cmd = new Subcommand(file, null)
                    const subcommandInstance = client.slashSubcommands.get(file)
                    subcommandInstance.groupCommands.set(cmd.name, cmd)
                    delete require.cache[require.resolve(path.join(filePath, file, subcommandFile))]
                }
            } catch (err) { console.log(`===============\nBIG ERR:\n${err}\n`) }
        }
        console.log('-> [Loaded SlashCommand]', file)
    }
}

async function registerEvents(client) {
    const filePath = path.join(__dirname, '../events')
    const folders = await fs.readdir(filePath)
    for (const folder of folders) {
        if (folder == 'Discord')
            await loadDiscordEvents(client, filePath, folder)
        if (folder == 'Player')
            await loadPlayerEvents(client, filePath, folder)
    }
}

async function registerButtons(client, interaction, dir = '../buttons') {
    const customId = JSON.parse(interaction.customId)
    const file_of_button = customId.ffb
    //const queue = useQueue(interaction.guild)
    if (file_of_button) {
        const filePath = path.join(__dirname, dir)
        delete require.cache[require.resolve(path.join(__dirname, `../buttons/${file_of_button}.js`))]
        const button = require(path.join(__dirname, `../buttons/${file_of_button}.js`))
        if (button)
            return button(interaction)
    }
}

module.exports = {
    registerCommands,
    registerSubcommands,
    registerEvents,
    registerButtons
}
