const BaseSlashCommand = require('../../utils/BaseSlashCommand')
const { EmbedBuilder, SlashCommandBuilder } = require('discord.js')
const { useQueue } = require('discord-player')

module.exports = class ShuffleSlashCommand extends BaseSlashCommand {
    // name: 'shuffle',
    // description: 'Shuffle the queue',
    // category: 'music',
    // voiceChannel: true,
    constructor() {
        super('shuffle', 'Shuffle the queue', 'music', true)
    }

    async run(client, inter) {
        await inter.deferReply({ ephemeral: true })
        const queue = useQueue(inter.guild)

        if (!queue || !queue.isPlaying())
            return inter.editReply({
                content: `No music currently playing ${inter.member}... try again ? ❌`
            })
        if (!queue.tracks.toArray()[0])
            return inter.editReply({
                content: `No music in the queue after the current one ${inter.member}... try again ? ❌`
            })
        await queue.tracks.shuffle()
        const ShuffleEmbed = new EmbedBuilder()
            .setColor('#2f3136')
            .setAuthor({name: `Queue shuffled ${queue.tracks.size} song(s)! ✅` })

        return inter.editReply({ embeds: [ShuffleEmbed] })
            .then(setTimeout(() => inter.deleteReply(), 5000))
    }

    getSlashCommandJSON() {
        return new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .toJSON()
    }
}