const { EmbedBuilder } = require('discord.js')

module.exports = (queue, error) => {
    const errDisplay = 'Bot had an unexpected error, please check the console imminently!'

    try {
        const ErrorEmbed = new EmbedBuilder()
            .setAuthor({name: errDisplay, iconURL: track.thumbnail})
            .setColor('#EE4B2B')
        queue.metadata['channel'].send({ embeds: [ErrorEmbed] })
    }   catch {
        const ErrorEmbed = new EmbedBuilder()
            .setAuthor({name: errDisplay})
            .setColor('#EE4B2B')
        queue.metadata['channel'].send({ embeds: [ErrorEmbed] })
    }

    console.log(`Error emitted from the PLayer ${error.message}`)
}
