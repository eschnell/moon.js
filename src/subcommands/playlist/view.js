const BaseSubcommandExecutor = require('../../utils/BaseSubcommandExecutor')
const makePlaylistEmbed = require('../../utils/makePlaylistEmbed')

module.exports = class ViewSubcommand extends BaseSubcommandExecutor {
    constructor(baseCommand, group) {
        super(baseCommand, group, 'view')
    }

    async run(client, inter) {
        await makePlaylistEmbed(inter, 0)
    }
}
